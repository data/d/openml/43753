# OpenML dataset: Country-Socioeconomic-Status-Scores-Part-II

https://www.openml.org/d/43753

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset contains estimates of the socioeconomic status (SES) position of each of 149 countries covering the period 1880-2010. Measures of SES, which are in decades, allow for a 130 year time-series analysis of the changing position of countries in the global status hierarchy. SES scores are the average of each countrys income and education ranking and are reported as percentile rankings ranging from 1-99. As such, they can be interpreted similarly to other percentile rankings, such has high school standardized test scores. If country A has an SES score of 55, for example, it indicates that 55 percent of the countries in this dataset have a lower average income and education ranking than country A. ISO alpha and numeric country codes are included to allow users to merge these data with other variables, such as those found in the World Banks World Development Indicators Database and the United Nations Common Database.
See here for a working example of how the data might be used to better understand how the world came to look the way it does, at least in terms of status position of countries. 
VARIABLE DESCRIPTIONS: 
unid: ISO numeric country code (used by the United Nations) 
wbid: ISO alpha country code (used by the World Bank) 
SES: Country socioeconomic status score (percentile) based on GDP per capita and educational attainment (n=174) 
country: Short country name 
year: Survey year 
gdppc: GDP per capita: Single time-series (imputed) 
yrseduc: Completed years of education in the adult (15+) population 
region5: Five category regional coding schema
regionUN: United Nations regional coding schema
DATA SOURCES: 
The dataset was compiled by Shawn Dorius (sdoriusiastate.edu) from a large number of data sources, listed below. GDP per Capita: 

Maddison, Angus. 2004. 'The World Economy: Historical Statistics'. Organization for Economic Co-operation and Development: Paris. GDP  GDP per capita data in (1990 Geary-Khamis dollars, PPPs of currencies and average prices of commodities). Maddison data collected from: http://www.ggdc.net/MADDISON/Historical_Statistics/horizontal-file_02-2010.xls. 
World Development Indicators Database Years of Education 1. Morrisson and Murtin.2009. 'The Century of Education'. Journal of Human Capital(3)1:1-42. Data downloaded from http://www.fabricemurtin.com/ 2. Cohen, Daniel  Marcelo Cohen. 2007. 'Growth and human capital: Good data, good results' Journal of economic growth 12(1):51-76. Data downloaded from http://soto.iae-csic.org/Data.htm 
Barro, Robert and Jong-Wha Lee, 2013, "A New Data Set of Educational Attainment in the World, 1950-2010." Journal of Development Economics, vol 104, pp.184-198. Data downloaded from http://www.barrolee.com/ 
Maddison, Angus. 2004. 'The World Economy: Historical Statistics'. Organization for Economic Co-operation and Development: Paris. 13. 
United Nations Population Division. 2009.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43753) of an [OpenML dataset](https://www.openml.org/d/43753). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43753/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43753/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43753/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

